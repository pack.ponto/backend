package br.com.alterdata.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.alterdata.domain.Colaborador;
import br.com.alterdata.exceptions.ColaboradorJaCadastradoException;
import br.com.alterdata.exceptions.ColaboradorNaoCadastradoException;
import br.com.alterdata.exceptions.ListaColaboradoresInexistenteException;
import br.com.alterdata.services.ColaboradorService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/colaboradores")
public class ColaboradorController {

    @Autowired
    private ColaboradorService service;

    @PostMapping("/obter-lista")
    public ResponseEntity<?> obterColaboradores() {
        List<String> colaboradores = new ArrayList<>();

        try {
            colaboradores = service.findAll();
        } catch (ListaColaboradoresInexistenteException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMensagem());
        }

        return ResponseEntity.status(HttpStatus.OK).body(colaboradores);
    }

    @PostMapping("/obter")
    public ResponseEntity<?> obterColaborador(@Valid @RequestBody Colaborador colaborador) {
        Colaborador existente = new Colaborador();

        try {
            existente = service.findByIdentificador(colaborador.getIdentificador());
        } catch (ColaboradorNaoCadastradoException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMensagem());
        }

        return ResponseEntity.status(HttpStatus.OK).body(existente);
    }

    @PostMapping("/cadastrar")
    public ResponseEntity<?> cadastrarColaborador(@Valid @RequestBody Colaborador colaborador) {
        try {
            service.save(colaborador);
        } catch (ColaboradorJaCadastradoException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMensagem());
        }

        return ResponseEntity.status(HttpStatus.CREATED).body("Novo colaborador cadastrado com sucesso.");
    }

}
