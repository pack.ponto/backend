package br.com.alterdata.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.alterdata.domain.Colaborador;
import br.com.alterdata.domain.Expediente;
import br.com.alterdata.dto.ExpedienteReponseDTO;
import br.com.alterdata.dto.ExpedienteRequestDTO;
import br.com.alterdata.dto.PontoRequestDTO;
import br.com.alterdata.enums.TipoPonto;
import br.com.alterdata.exceptions.ColaboradorNaoCadastradoException;
import br.com.alterdata.exceptions.ExpedienteInexistenteException;
import br.com.alterdata.exceptions.PontoJaRegistradoException;
import br.com.alterdata.exceptions.SemPontoAnteriorException;
import br.com.alterdata.services.ColaboradorService;
import br.com.alterdata.services.ExpedienteService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/expediente")
public class ExpedienteController {

    @Autowired
    private ExpedienteService expedienteService;

    @Autowired
    private ColaboradorService colaboradorService;

    @PostMapping("/atual")
    public ResponseEntity<?> obterExpedienteDoDia(@Valid @RequestBody Colaborador colaborador) {

        ExpedienteReponseDTO atualDto;

        try {
            atualDto = expedienteService.findByData(colaborador);
        } catch (ExpedienteInexistenteException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMensagem());
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(atualDto);
    }

    @PostMapping("/mensal")
    public ResponseEntity<?> obterExpedienteDoMes(@Valid @RequestBody ExpedienteRequestDTO expedienteRequestDto) {

        List<ExpedienteReponseDTO> expedienteMensalDto = new ArrayList<>();

        try {
            colaboradorService.findByIdentificador(expedienteRequestDto.getIdentificadorColaborador());
            expedienteMensalDto = expedienteService.findByMesAno(expedienteRequestDto);
        } catch (ColaboradorNaoCadastradoException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMensagem());
        } catch (ExpedienteInexistenteException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMensagem());
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(expedienteMensalDto);
    }

    @PostMapping("/registrar-entrada")
    public ResponseEntity<?> registrarEntrada(@Valid @RequestBody PontoRequestDTO entrada) {

        Expediente novo = new Expediente();

        try {
            int ponto = 0;
            expedienteService.verificarExistenciaPonto(entrada, ponto);
            Colaborador colaborador = colaboradorService.findByIdentificador(entrada.getIdentificadorColaborador());
            novo = expedienteService.criarNovoExpediente(colaborador, entrada);
        } catch (PontoJaRegistradoException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMensagem());
        } catch (ColaboradorNaoCadastradoException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMensagem());
        } catch (ExpedienteInexistenteException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMensagem());
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(novo);
    }

    @PatchMapping("/registrar-almoco")
    public ResponseEntity<?> registrarHoraDoAlmoco(@Valid @RequestBody PontoRequestDTO almoco) {

        Expediente atualizado = new Expediente();

        try {
            int ponto = 1;
            expedienteService.verificarExistenciaPonto(almoco, ponto);
            atualizado = expedienteService.registrarPonto(almoco, ponto - 1, TipoPonto.ALMOCO);
        } catch (PontoJaRegistradoException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMensagem());
        } catch (SemPontoAnteriorException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMensagem());
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(atualizado);
    }

    @PatchMapping("/registrar-retorno")
    public ResponseEntity<?> registrarRetorno(@Valid @RequestBody PontoRequestDTO retorno) {

        Expediente atualizado = new Expediente();

        try {
            int ponto = 2;
            expedienteService.verificarExistenciaPonto(retorno, ponto);
            atualizado = expedienteService.registrarPonto(retorno, ponto - 1, TipoPonto.RETORNO);
        } catch (PontoJaRegistradoException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMensagem());
        } catch (SemPontoAnteriorException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMensagem());
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(atualizado);
    }

    @PatchMapping("/registrar-saida")
    public ResponseEntity<?> registrarSaida(@Valid @RequestBody PontoRequestDTO saida) {

        Expediente atualizado = new Expediente();

        try {
            int ponto = 3;
            expedienteService.verificarExistenciaPonto(saida, ponto);
            atualizado = expedienteService.registrarPonto(saida, ponto - 1, TipoPonto.SAIDA);
        } catch (PontoJaRegistradoException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMensagem());
        } catch (SemPontoAnteriorException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMensagem());
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(atualizado);
    }

}
