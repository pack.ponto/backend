package br.com.alterdata.domain;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import br.com.alterdata.enums.TipoPonto;

@Entity
public class Ponto implements Serializable {

    private static final long serialVersionUID = 8533131104685148763L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private TipoPonto tipo;

    @NotNull
    private LocalDateTime horario;

    @Size(max = 60)
    private String observacoes;

    @ManyToOne
    @JoinColumn(name = "expediente_id", nullable = false)
    @NotNull
    @JsonProperty(access = Access.WRITE_ONLY)
    private Expediente expediente;

    public Ponto() {
        super();
    }

    public Ponto(@NotNull TipoPonto tipo, @NotNull LocalDateTime horario, @Size(max = 60) String observacoes,
                 @NotNull Expediente expediente) {
        super();
        this.tipo = tipo;
        this.horario = horario;
        this.observacoes = observacoes;
        this.expediente = expediente;
    }

    public Long getId() {
        return id;
    }

    public TipoPonto getTipo() {
        return tipo;
    }

    public LocalDateTime getHorario() {
        return horario;
    }

    public String getObservacoes() {
        return observacoes;
    }

    public Expediente getExpediente() {
        return expediente;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setTipo(TipoPonto tipo) {
        this.tipo = tipo;
    }

    public void setHorario(LocalDateTime horario) {
        this.horario = horario;
    }

    public void setObservacoes(String observacoes) {
        this.observacoes = observacoes;
    }

    public void setExpediente(Expediente expediente) {
        this.expediente = expediente;
    }

}
