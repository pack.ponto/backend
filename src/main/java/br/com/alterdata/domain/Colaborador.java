package br.com.alterdata.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Colaborador implements Serializable {

    private static final long serialVersionUID = 564953389485775022L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(unique = true)
    @Size(max = 30)
    private String identificador;

    @OneToMany(mappedBy = "colaborador")
    private List<Expediente> expedientes;

    public Colaborador() {
        super();
    }

    public Colaborador(@NotNull @Size(max = 30) String identificador, List<Expediente> expedientes) {
        super();
        this.identificador = identificador;
        this.expedientes = expedientes;
    }

    public Long getId() {
        return id;
    }

    public String getIdentificador() {
        return identificador;
    }

    public List<Expediente> getExpedientes() {
        return expedientes;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public void setExpedientes(List<Expediente> expedientes) {
        this.expedientes = expedientes;
    }
}
