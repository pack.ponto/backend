package br.com.alterdata.domain;

import java.io.Serializable;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Collections;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import br.com.alterdata.enums.Status;

@Entity
public class Expediente implements Serializable {

    private static final long serialVersionUID = 1106672570877386353L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany(mappedBy = "expediente")
    private List<Ponto> pontos;

    @ManyToOne
    @JoinColumn(name = "colaborador_id", nullable = false)
    @NotNull
    @JsonProperty(access = Access.WRITE_ONLY)
    private Colaborador colaborador;

    @NotNull
    private LocalTime horasTrabalhadas;

    @NotNull
    private Status status;

    public Expediente() {
        super();
    }

    public Expediente(List<Ponto> pontos, @NotNull Colaborador colaborador, @NotNull LocalTime horasTrabalhadas,
                      @NotNull Status status) {
        super();
        this.pontos = pontos;
        this.colaborador = colaborador;
        this.horasTrabalhadas = horasTrabalhadas;
        this.status = status;
    }

    public void calcularHorasTrabalhadas() {

        Collections.reverse(pontos);

        LocalDateTime pontoPosterior = pontos.get(0).getHorario().withSecond(0).withNano(0);
        LocalDateTime pontoAnterior = pontos.get(1).getHorario().withSecond(0).withNano(0);

        Duration duracao = Duration.between(pontoAnterior, pontoPosterior);

        int horas = (int) duracao.toHours();
        int minutos = (int) (duracao.toMinutes() - horas * 60);

        int horasSomadas = horasTrabalhadas.getHour() + horas;
        int minutosSomados = horasTrabalhadas.getMinute() + minutos;

        if (minutosSomados >= 60) {
            int minutosHora = minutosSomados / 60;
            int minutosMinuto = minutosSomados % 60;
            horasSomadas += minutosHora;
            minutosSomados = minutosMinuto;
        }

        horasTrabalhadas = LocalTime.of(horasSomadas, minutosSomados);

        Collections.reverse(pontos);

    }

    public void definirStatus() {

        LocalTime expedienteHoraExtra = LocalTime.of(8, 05);
        LocalTime expedienteNormal = LocalTime.of(7, 55);

        int maisHoras = horasTrabalhadas.compareTo(expedienteHoraExtra);
        int horasSuficientes = horasTrabalhadas.compareTo(expedienteNormal);

        if (maisHoras > 0) {
            status = Status.HORA_EXTRA;
        } else if (horasSuficientes >= 0 && maisHoras <= 0) {
            status = Status.NORMAL;
        }
    }

    public Long getId() {
        return id;
    }

    public List<Ponto> getPontos() {
        return pontos;
    }

    public Colaborador getColaborador() {
        return colaborador;
    }

    public LocalTime getHorasTrabalhadas() {
        return horasTrabalhadas;
    }

    public Status getStatus() {
        return status;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setPontos(List<Ponto> pontos) {
        this.pontos = pontos;
    }

    public void setColaborador(Colaborador colaborador) {
        this.colaborador = colaborador;
    }

    public void setHorasTrabalhadas(LocalTime horasTrabalhadas) {
        this.horasTrabalhadas = horasTrabalhadas;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

}
