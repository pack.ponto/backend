package br.com.alterdata.enums;

public enum Status {

    NORMAL(1), HORA_EXTRA(2), ATRASO(3);

    private int id;

    private Status(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

}