package br.com.alterdata.enums;

public enum TipoPonto {

    ENTRADA(1), ALMOCO(2), RETORNO(3), SAIDA(4);

    private int id;

    private TipoPonto(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

}
