package br.com.alterdata.services;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.alterdata.domain.Colaborador;
import br.com.alterdata.domain.Expediente;
import br.com.alterdata.domain.Ponto;
import br.com.alterdata.dto.ExpedienteReponseDTO;
import br.com.alterdata.dto.ExpedienteRequestDTO;
import br.com.alterdata.dto.PontoRequestDTO;
import br.com.alterdata.enums.Status;
import br.com.alterdata.enums.TipoPonto;
import br.com.alterdata.exceptions.ExpedienteInexistenteException;
import br.com.alterdata.exceptions.PontoJaRegistradoException;
import br.com.alterdata.exceptions.SemPontoAnteriorException;
import br.com.alterdata.repository.ExpedienteRepository;
import br.com.alterdata.repository.PontoRepository;

@Service
public class ExpedienteService {

    @Autowired
    private ExpedienteRepository expedienteRepository;

    @Autowired
    private PontoRepository pontoRepository;

    @Transactional
    public ExpedienteReponseDTO findByData(Colaborador colaborador) throws ExpedienteInexistenteException {
        Expediente atual = expedienteRepository.findByData(colaborador.getIdentificador(), LocalDate.now(),
                LocalDate.now().plusDays(1));

        if (atual == null) {
            throw new ExpedienteInexistenteException();
        }

        return ExpedienteReponseDTO.fromExpediente(atual);
    }

    @Transactional
    public List<ExpedienteReponseDTO> findByMesAno(ExpedienteRequestDTO expedienteRequestDto)
    throws ExpedienteInexistenteException {
        List<ExpedienteReponseDTO> expedienteMensalDto = new ArrayList<>();

        List<Expediente> expedienteMensal = expedienteRepository.findByMesAno(
                expedienteRequestDto.getIdentificadorColaborador(), expedienteRequestDto.getMes(),
                expedienteRequestDto.getAno());

        if (expedienteMensal == null) {
            throw new ExpedienteInexistenteException();
        }

        for (Expediente expediente : expedienteMensal) {
            ExpedienteReponseDTO expedienteResponseDto = ExpedienteReponseDTO.fromExpediente(expediente);
            expedienteMensalDto.add(expedienteResponseDto);
        }

        return expedienteMensalDto;
    }

    @Transactional
    public void verificarExistenciaPonto(PontoRequestDTO pontoDto, int ponto) throws PontoJaRegistradoException {
        Expediente existente = expedienteRepository.findByPonto(pontoDto.getIdentificadorColaborador(), ponto,
                LocalDate.now(), LocalDate.now().plusDays(1));

        if (existente != null) {
            throw new PontoJaRegistradoException();
        }
    }

    @Transactional
    public Expediente criarNovoExpediente(Colaborador colaborador, 
                                          PontoRequestDTO entrada) throws ExpedienteInexistenteException {
        Expediente novo = new Expediente();

        novo.setColaborador(colaborador);
        novo.setHorasTrabalhadas(LocalTime.of(00, 00));
        novo.setStatus(Status.ATRASO);

        Long id = expedienteRepository.save(novo).getId();
        Optional<Expediente> expedienteOpt = expedienteRepository.findById(id);

        if (expedienteOpt.isPresent()) {
            novo = expedienteOpt.get();
            Ponto ponto = new Ponto(TipoPonto.ENTRADA, LocalDateTime.now(), entrada.getObservacoes(), novo);

            pontoRepository.save(ponto);

            List<Ponto> pontos = new ArrayList<>();

            pontos.add(ponto);
            novo.setPontos(pontos);

            return novo;
        } else {
            throw new ExpedienteInexistenteException();
        }
    }

    @Transactional
    public Expediente registrarPonto(PontoRequestDTO pontoDto, int pontoAnterior, 
                                     TipoPonto tipoPonto) throws SemPontoAnteriorException {
        Expediente atualizado = expedienteRepository.findByPonto(pontoDto.getIdentificadorColaborador(), pontoAnterior,
                LocalDate.now(), LocalDate.now().plusDays(1));

        if (atualizado == null) {
            throw new SemPontoAnteriorException();
        } else {
            Ponto ponto = new Ponto(tipoPonto, LocalDateTime.now(), pontoDto.getObservacoes(), atualizado);
            pontoRepository.save(ponto);
            
            if (tipoPonto != TipoPonto.RETORNO) {                
                atualizado.calcularHorasTrabalhadas();
            }
            
            atualizado.definirStatus();
            expedienteRepository.save(atualizado);

            return atualizado;
        }
    }

}
