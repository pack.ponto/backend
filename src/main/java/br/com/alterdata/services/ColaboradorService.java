package br.com.alterdata.services;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.alterdata.domain.Colaborador;
import br.com.alterdata.exceptions.ColaboradorJaCadastradoException;
import br.com.alterdata.exceptions.ColaboradorNaoCadastradoException;
import br.com.alterdata.exceptions.ListaColaboradoresInexistenteException;
import br.com.alterdata.repository.ColaboradorRepository;

@Service
public class ColaboradorService {

    @Autowired
    private ColaboradorRepository repository;

    @Transactional
    public List<String> findAll() throws ListaColaboradoresInexistenteException {
        List<Colaborador> colaboradores = new ArrayList<>();
        colaboradores = repository.findAll();

        if (colaboradores == null) {
            throw new ListaColaboradoresInexistenteException();
        }

        List<String> identificadores = new ArrayList<>();

        for (Colaborador colaborador : colaboradores) {
            String identificador = colaborador.getIdentificador();
            identificadores.add(identificador);
        }

        java.util.Collections.sort(identificadores);

        return identificadores;
    }

    @Transactional
    public Colaborador findByIdentificador(String identificador) throws ColaboradorNaoCadastradoException {

        Colaborador colaborador = repository.findByIdentificador(identificador);

        if (colaborador == null) {
            throw new ColaboradorNaoCadastradoException();
        } else {
            return colaborador;
        }
    }

    @Transactional
    public void save(Colaborador colaborador) throws ColaboradorJaCadastradoException {

        Colaborador existente = repository.findByIdentificador(colaborador.getIdentificador());

        if (existente == null) {
            repository.save(colaborador);
        } else {
            throw new ColaboradorJaCadastradoException();
        }

    }

}
