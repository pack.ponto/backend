package br.com.alterdata.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.alterdata.domain.Expediente;

public interface ExpedienteRepository extends JpaRepository<Expediente, Long> {

    @Query(value = "SELECT * FROM expediente\r\n"
            + "LEFT JOIN colaborador ON expediente.colaborador_id=colaborador.id\r\n"
            + "LEFT JOIN ponto ON expediente.id = ponto.expediente_id\r\n"
            + "WHERE colaborador.identificador = :colaboradorIdentificador AND tipo = :tipoPonto\r\n"
            + "AND horario BETWEEN :dataInicio AND :dataFim", nativeQuery = true)
    Expediente findByPonto(@Param("colaboradorIdentificador") String identificador, @Param("tipoPonto") int tipoPonto,
                           @Param("dataInicio") LocalDate dataInicio, @Param("dataFim") LocalDate dataFim);

    @Query(value = "SELECT * FROM expediente \r\n"
            + "LEFT JOIN colaborador ON expediente.colaborador_id=colaborador.id\r\n"
            + "LEFT JOIN ponto ON expediente.id = ponto.expediente_id\r\n"
            + "WHERE colaborador.identificador = :colaboradorIdentificador\r\n"
            + "AND horario BETWEEN :dataInicio AND :dataFim", nativeQuery = true)
    Expediente findByData(@Param("colaboradorIdentificador") String identificador,
                          @Param("dataInicio") LocalDate dataInicio, @Param("dataFim") LocalDate dataFim);

    @Query(value = "SELECT * FROM expediente\r\n"
            + "LEFT JOIN colaborador ON expediente.colaborador_id=colaborador.id\r\n"
            + "LEFT JOIN ponto ON expediente.id = ponto.expediente_id\r\n"
            + "WHERE colaborador.identificador = :colaboradorIdentificador AND tipo = 0\r\n"
            + "AND EXTRACT(MONTH FROM ponto.horario) = :mes\r\n"
            + "AND EXTRACT(YEAR FROM ponto.horario) = :ano", nativeQuery = true)
    List<Expediente> findByMesAno(@Param("colaboradorIdentificador") String identificador, @Param("mes") int mes,
                                  @Param("ano") int ano);

}
