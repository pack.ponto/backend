package br.com.alterdata.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.alterdata.domain.Ponto;

public interface PontoRepository extends JpaRepository<Ponto, Long> {

}
