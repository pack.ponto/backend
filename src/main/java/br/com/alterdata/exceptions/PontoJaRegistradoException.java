package br.com.alterdata.exceptions;

public class PontoJaRegistradoException extends Exception {

    private static final long serialVersionUID = -5247305878088882090L;

    private String mensagem = "Ponto já registrado.";

    public PontoJaRegistradoException() {
        super();
    }

    public String getMensagem() {
        return mensagem;
    }
}
