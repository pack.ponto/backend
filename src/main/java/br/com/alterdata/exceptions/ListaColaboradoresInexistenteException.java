package br.com.alterdata.exceptions;

public class ListaColaboradoresInexistenteException extends Exception {

    private static final long serialVersionUID = 8824980321855969359L;
    
    private String mensagem = "Lista de colaboradores não existente.";

    public ListaColaboradoresInexistenteException() {
        super();
    }

    public String getMensagem() {
        return mensagem;
    }

}
