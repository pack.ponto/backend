package br.com.alterdata.exceptions;

public class SemPontoAnteriorException extends Exception {

    private static final long serialVersionUID = 7667561439329120488L;

    private String mensagem = "Ponto anterior não foi registrado.";

    public SemPontoAnteriorException() {
        super();
    }

    public String getMensagem() {
        return mensagem;
    }

}
