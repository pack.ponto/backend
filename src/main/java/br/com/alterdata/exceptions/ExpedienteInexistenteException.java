package br.com.alterdata.exceptions;

public class ExpedienteInexistenteException extends Exception {

    private static final long serialVersionUID = -3938996625710656813L;

    private String mensagem = "Expediente não existe.";

    public ExpedienteInexistenteException() {
        super();
    }

    public String getMensagem() {
        return mensagem;
    }
}