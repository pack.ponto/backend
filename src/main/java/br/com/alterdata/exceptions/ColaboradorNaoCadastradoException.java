package br.com.alterdata.exceptions;

public class ColaboradorNaoCadastradoException extends Exception {

    private static final long serialVersionUID = -8706633592139771345L;

    private String mensagem = "Colaborador não cadastrado.";

    public ColaboradorNaoCadastradoException() {
        super();
    }

    public String getMensagem() {
        return mensagem;
    }
}
