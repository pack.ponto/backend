package br.com.alterdata.exceptions;

public class ColaboradorJaCadastradoException extends Exception {

    private static final long serialVersionUID = 6453242247964131662L;

    private String mensagem = "Colaborador já cadastrado.";

    public ColaboradorJaCadastradoException() {
        super();
    }

    public String getMensagem() {
        return mensagem;
    }

}
