package br.com.alterdata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * @copyright Alterdata Software
 * @author marcela.dsn.pack
 * @since 19/12/2020
 * @description sistema para controle de ponto dos colaboradores da alterdata software
*/
@SpringBootApplication
public class PontoApplication {

    public static void main(String[] args) {
        SpringApplication.run(PontoApplication.class, args);
    }

}
