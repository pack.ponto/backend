package br.com.alterdata.dto;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.com.alterdata.enums.TipoPonto;

public class PontoReponseDTO {

    @NotNull
    private String tipo;

    @NotNull
    private String horario;

    @Size(max = 60)
    private String observacoes;

    public PontoReponseDTO() {
        super();
    }

    public PontoReponseDTO(@NotNull String tipo, @NotNull String horario, @Size(max = 60) String observacoes) {
        super();
        this.tipo = tipo;
        this.horario = horario;
        this.observacoes = observacoes;
    }

    public String getTipo() {
        return tipo;
    }

    public String getHorario() {
        return horario;
    }

    public String getObservacoes() {
        return observacoes;
    }

    public void setTipo(TipoPonto enumTipo) {

        switch (enumTipo) {
        case ALMOCO:
            this.tipo = "Almoço";
            break;
        case RETORNO:
            this.tipo = "Retorno";
            break;
        case SAIDA:
            this.tipo = "Saída";
            break;
        default:
            this.tipo = "Entrada";
            break;
        }

    }

    public void setHorario(LocalDateTime dateHorario) {

        DateTimeFormatter formatador = DateTimeFormatter.ofPattern("HH:mm");

        String stringHorario = dateHorario.format(formatador);

        this.horario = stringHorario;
    }

    public void setObservacoes(String observacoes) {
        this.observacoes = observacoes;
    }

}
