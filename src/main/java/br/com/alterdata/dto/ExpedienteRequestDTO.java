package br.com.alterdata.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ExpedienteRequestDTO {

    @NotNull
    @Size(max = 30)
    private String identificadorColaborador;

    @NotNull
    private int mes;

    @NotNull
    private int ano;

    public ExpedienteRequestDTO() {
        super();
    }

    public ExpedienteRequestDTO(@NotNull @Size(max = 30) String identificadorColaborador, @NotNull int mes,
                                @NotNull int ano) {
        super();
        this.identificadorColaborador = identificadorColaborador;
        this.mes = mes;
        this.ano = ano;
    }

    public String getIdentificadorColaborador() {
        return identificadorColaborador;
    }

    public int getMes() {
        return mes;
    }

    public int getAno() {
        return ano;
    }

    public void setIdentificadorColaborador(String identificadorColaborador) {
        this.identificadorColaborador = identificadorColaborador;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

}
