package br.com.alterdata.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class PontoRequestDTO {

    @NotNull
    @Size(max = 30)
    private String identificadorColaborador;

    @Size(max = 60)
    String observacoes;

    public PontoRequestDTO() {
        super();
    }

    public PontoRequestDTO(@NotNull @Size(max = 30) String identificadorColaborador,
                           @Size(max = 60) String observacoes) {
        super();
        this.identificadorColaborador = identificadorColaborador;
        this.observacoes = observacoes;
    }

    public String getIdentificadorColaborador() {
        return identificadorColaborador;
    }

    public String getObservacoes() {
        return observacoes;
    }

    public void setIdentificadorColaborador(String identificadorColaborador) {
        this.identificadorColaborador = identificadorColaborador;
    }

    public void setObservacoes(String observacoes) {
        this.observacoes = observacoes;
    }

}
