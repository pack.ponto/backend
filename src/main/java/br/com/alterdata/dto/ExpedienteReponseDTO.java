package br.com.alterdata.dto;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import br.com.alterdata.domain.Expediente;
import br.com.alterdata.domain.Ponto;
import br.com.alterdata.enums.Status;

public class ExpedienteReponseDTO {

    @NotNull
    private LocalDateTime data;

    @NotNull
    private List<PontoReponseDTO> pontos;

    @NotNull
    private String horasTrabalhadas;

    @NotNull
    private String status;

    public ExpedienteReponseDTO() {
        super();
    }

    public ExpedienteReponseDTO(@NotNull LocalDateTime data, @NotNull List<PontoReponseDTO> pontos,
                                @NotNull String horasTrabalhadas, @NotNull String status) {
        super();
        this.data = data;
        this.pontos = pontos;
        this.horasTrabalhadas = horasTrabalhadas;
        this.status = status;
    }

    public static ExpedienteReponseDTO fromExpediente(Expediente expediente) {

        ExpedienteReponseDTO dto = new ExpedienteReponseDTO();

        Ponto primeiro = expediente.getPontos().get(0);
        dto.setData(primeiro.getHorario());
        dto.setHorasTrabalhadas(expediente.getHorasTrabalhadas());
        dto.setStatus(expediente.getStatus());

        dto.pontos = new ArrayList<>();

        for (Ponto ponto : expediente.getPontos()) {

            PontoReponseDTO pontoDto = new PontoReponseDTO();
            pontoDto.setTipo(ponto.getTipo());
            pontoDto.setHorario(ponto.getHorario());
            pontoDto.setObservacoes(ponto.getObservacoes());
            dto.getPontos().add(pontoDto);

        }

        return dto;
    }

    public LocalDateTime getData() {
        return data;
    }

    public List<PontoReponseDTO> getPontos() {
        return pontos;
    }

    public String getHorasTrabalhadas() {
        return horasTrabalhadas;
    }

    public String getStatus() {
        return status;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }

    public void setPontos(List<PontoReponseDTO> pontos) {
        this.pontos = pontos;
    }

    public void setHorasTrabalhadas(LocalTime horasTrabalhadas) {

        String horas = horasTrabalhadas.getHour() + "h";
        String minutos = horasTrabalhadas.getMinute() + "min";

        this.horasTrabalhadas = horas + " " + minutos;
    }

    public void setStatus(Status enumStatus) {

        switch (enumStatus) {
        case NORMAL:
            this.status = "Normal";
            break;
        case HORA_EXTRA:
            this.status = "Hora extra";
            break;
        default:
            this.status = "Atraso";
            break;
        }
        
    }

}
