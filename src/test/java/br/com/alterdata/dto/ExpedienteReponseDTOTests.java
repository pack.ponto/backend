package br.com.alterdata.dto;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.alterdata.domain.Colaborador;
import br.com.alterdata.domain.Expediente;
import br.com.alterdata.domain.Ponto;
import br.com.alterdata.enums.Status;
import br.com.alterdata.enums.TipoPonto;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ExpedienteReponseDTOTests {

    @Test
    public void deve_transformar_expediente_em_expedienteResponseDTO() {

        Ponto ponto = new Ponto();
        ponto.setHorario(LocalDateTime.now());
        ponto.setTipo(TipoPonto.ENTRADA);

        List<Ponto> pontos = new ArrayList<>();
        pontos.add(ponto);

        Colaborador colaborador = new Colaborador();
        colaborador.setIdentificador("pessoa");

        Expediente expediente = new Expediente(pontos, colaborador, LocalTime.of(8, 0), Status.NORMAL);

        PontoReponseDTO pontoDto = new PontoReponseDTO();
        pontoDto.setHorario(LocalDateTime.now());
        pontoDto.setTipo(TipoPonto.ENTRADA);

        List<PontoReponseDTO> pontosEsperado = new ArrayList<>();
        pontosEsperado.add(pontoDto);

        ExpedienteReponseDTO esperado = new ExpedienteReponseDTO(ponto.getHorario(), pontosEsperado, "8h 0min",
                "Normal");
        ExpedienteReponseDTO atual = ExpedienteReponseDTO.fromExpediente(expediente);

        assertThat(atual).isEqualToComparingFieldByFieldRecursively(esperado);
    }

}